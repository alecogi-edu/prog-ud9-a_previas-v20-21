package es.coloma;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Actividad7. Crea un método getEdad() que pida al usuario un
 * número entre 10 y 50. Si el número no se encuentra en el rango,
 * deberá lanzar una Excepción de tipo InputMismatchException
 * con un mensaje descriptivo del error que se ha producido.
 * Captura desde el main() la Excepción anterior y muestra el
 * mensaje.
 *
 * Prueba el programa para los valores 10, 20, 50, 200 y 9.
 */
public class Actividad7 {

    private static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            int edad = getEdad();
            System.out.println(edad);
        }catch (InputMismatchException e) {
            System.out.println(e.getMessage());
        }
    }

    private static int getEdad() {
        int edad = teclado.nextInt();
        if (edad < 10 || edad > 50) {
            throw new InputMismatchException("La edad debe estar entre 10 y 50");
        }
        return edad;
    }

}

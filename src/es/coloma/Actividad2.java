package es.coloma;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad2 {

    public static void main(String[] args) {
        for (int i = 0; i < 6; i++) {
            try {
                obtenerNumero();
            } catch (InputMismatchException e) {
                System.out.println(e.getMessage());
                i--;
            }
        }
    }

    private static int obtenerNumero(){
        Scanner teclado = new Scanner(System.in);
        System.out.println("Introduce un número");
        return teclado.nextInt();
    }
}

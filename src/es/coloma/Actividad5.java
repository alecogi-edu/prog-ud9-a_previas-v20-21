package es.coloma;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Activitat5.- Con el fin de comprobar el funcionamiento de algunas excepciones, haz lo siguiente:
 * • Crea un método que reciba un array de enteros y un entero que represente el número de elementos del array. Este método irá leyendo números desde teclado y guardándolos en un array.
 * ¿Qué situaciones se pueden producir y qué hacer en ellas?
 * ◦ Que se inserte una letra: informar al usuario.
 * ◦ Que se sobrepase la capacidad del array: Informar al usuario, finalizar y mostrar el contenido del array.
 * ◦ Que el array no haya sido inicializado: informar al usuario y finalizar.
 * • Desde el main() Crea un array de 5 números enteros y llama al método. Haz diferentes
 *   pruebas para forzar las dos primeras excepciones.
 * • Después declara (Sin crearlo) una variable de tipo array en el main() y (sin inicializarla) llama al método.
 */
public class Actividad5 {

    public static void main(String[] args) {

        //case valido
        int[] array5 = new int[5];
        readNumbers(array5, 5);

        //case invalido
        readNumbers(array5, 6);

        //case invalido
        int[] array2 = null;
        readNumbers(array2, 10);

    }

    public static void readNumbers(int[] numbers, int numElements) {
        Scanner input = new Scanner(System.in);
        for (int i = 0; i < numElements; i++) {
            try {
                System.out.println("Introduce un número");
                numbers[i] = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Debe introducir un número");
                input.nextLine();
                i--;
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("El array ya està completo");
                return;
            } catch (NullPointerException e) {
                System.out.println("El array no esta inicialitzat");
                return;
            }
        }

    }
}


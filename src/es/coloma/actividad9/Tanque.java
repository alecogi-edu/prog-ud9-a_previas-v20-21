package es.coloma.actividad9;

import es.coloma.actividad9.exception.TanqueLLenoExcepcion;
import es.coloma.actividad9.exception.TanqueVacioExcepcion;

public class Tanque {

    private int capacidad;

    private int carga;

    Tanque (int capacidad) {
        this.capacidad = capacidad;
        this.carga = 0;
    }

    public void agregarCarga(int cantidad) throws TanqueLLenoExcepcion {
        if ((this.carga + cantidad) > capacidad ){
            throw new TanqueLLenoExcepcion();
        }
        this.carga += cantidad;
    }

    public void retirarCarga(int cantidad) throws TanqueVacioExcepcion {
        if ((this.carga - cantidad) < 0 ){
            throw new TanqueVacioExcepcion("No es posible retirar " + cantidad + "litros");
        }
        this.carga -= cantidad;
    }

}

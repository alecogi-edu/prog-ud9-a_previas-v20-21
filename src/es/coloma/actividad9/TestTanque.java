package es.coloma.actividad9;

import es.coloma.actividad9.exception.TanqueLLenoExcepcion;
import es.coloma.actividad9.exception.TanqueVacioExcepcion;

public class TestTanque {

    public static void main(String[] args) {

        Tanque tanqueAgua = new Tanque(100);

        try {
            System.out.println("Agrego 50 litros");
            tanqueAgua.agregarCarga(50);
            System.out.println("Agrego 200 litros");
            tanqueAgua.agregarCarga(200);
            System.out.println("Retiro 50 litros");
            tanqueAgua.retirarCarga(50);
            System.out.println("Retiro 300 litros");
            tanqueAgua.retirarCarga(300);
        } catch (TanqueLLenoExcepcion | TanqueVacioExcepcion excepcion ) {
            System.out.println(excepcion.getMessage());
        }

    }
}

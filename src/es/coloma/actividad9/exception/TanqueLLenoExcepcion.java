package es.coloma.actividad9.exception;

public class TanqueLLenoExcepcion extends Exception {

    public TanqueLLenoExcepcion() {
        super("El tanque está lleno");
    }

}

package es.coloma.actividad9.exception;

public class TanqueVacioExcepcion extends Exception {

    public TanqueVacioExcepcion() {
        super("El tanque está vacío");
    }

    public TanqueVacioExcepcion(String message) {
        super(message);
    }

}

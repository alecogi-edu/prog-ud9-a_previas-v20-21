package es.coloma.actividad6;

import java.util.ArrayList;
import java.util.Collections;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *  Activitat6.- Haz una programa con los siguientes requerimientos:
 *  • Queremos crear 2 alumnos. De cada alumno guardaremos el nombre, la edad y la altura.
 *  • Desde el main, solicita los datos al usuario y comprueba que a la hora de introducir la edad y la altura no se inserten letras.
 *    Si es así, repite el proceso hasta que se insertan números.
 *  • Crea los 2 alumnos desde el main(). Ver la información de cada uno de ellos y luego indica qué alumno es mayor que el otro alumno.
 *   (Hazlo implementado la interfaz comparable
 */
public class Actividad6 {

    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Estudiante 1");
        Student student1 = getNewStudent();
        System.out.println("Estudiante 2");
        Student student2 = getNewStudent();

        System.out.println(student1);
        System.out.println(student2);

        if (student1.compareTo(student2) > 0) {
            System.out.printf("El estudiante %s es major que el estudiant %s\n",
                    student1.getName(), student2.getName());
        } else if(student1.compareTo(student2) < 0){
            System.out.printf("El estudiante %s es major que el estudiant %s\n",
                    student2.getName(), student1.getName());
        } else {
            System.out.println("Son iguales");
        }
        ArrayList<Student> alumnosDaw = new ArrayList<>();
        alumnosDaw.add(student1);
        alumnosDaw.add(student2);
        Collections.sort(alumnosDaw);
        System.out.println(alumnosDaw);
    }

    private static Student getNewStudent() {
        String name = getInputName();
        int age = getInputAge();
        float height = getInputHeight();
        return new Student(name, age, height);
    }

    private static String getInputName(){
        System.out.println("Introduce el nombre");
        return input.next();
    }

    private static int getInputAge() {
        do {
            try {
                System.out.println("Introduce la edad");
                return input.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Debe introducir una edad válida");
            } finally {
                input.nextLine();
            }
        }while (true);
    }

    private static float getInputHeight() {
        try{
            System.out.println("Introduce la altura");
            return input.nextFloat();
        }catch (InputMismatchException e){
            System.out.println("Debe introducir una altura válida");
            return getInputHeight();
        } finally {
            input.nextLine();
        }

    }

}


package es.coloma.actividad6;

public class Student implements Comparable<Student> {

    private String name;

    private int age;

    private float height;

    public Student(String name, int age, float height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public String getName() {

        return this.name;

    }

    @Override
    public int compareTo(Student o) {
        if (this.age < o.age){
            return -1;
        }else if(this.age > o.age){
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "{" + "name: '" + name + ", age:" + age + ", height:" + height + "}";
    }
}
package es.coloma.actividad10.types;

public class Dog extends Animal{

    public Dog(Food food, Size size, String location) {
        super(food, size, location, "Perro");
    }

    @Override
    public void makeNoyse() {
        System.out.println("GUAU!!!");
    }
}

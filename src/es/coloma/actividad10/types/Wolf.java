package es.coloma.actividad10.types;

public class Wolf extends Animal{

    public Wolf(Food food, Size size, String location) {
        super(food, size, location, "Lobo");
    }

    @Override
    public void makeNoyse() {
        System.out.println("AUUUUUUUHHHH!!!!!!!!!");
    }
}

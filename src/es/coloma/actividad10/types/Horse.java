package es.coloma.actividad10.types;

public class Horse extends Animal {

    public Horse(Food food, Size size, String location) {
        super(food, size, location, "Caballo");
    }

    @Override
    public void makeNoyse() {
        System.out.println("IIIIIIH....!");
    }
}

package es.coloma.actividad10.types;

import es.coloma.actividad10.exception.ApareamientoInvalidoExcepcion;

public abstract class Animal {

    public enum Food {GRASS, MEAT}

    public enum Size {BIG, MEDIUM, SMALL};

    private String imageUrl;

    private Food food;

    private Size size;

    private String location;

    private String type;

    private int hungry;

    private Animal pareja;

    public Animal(Food food, Size size, String location, String type) {
        this.food = food;
        this.size = size;
        this.location = location;
        this.type = type;
        this.hungry = 0;
    }

    public void eat() {
        hungry = 0;
    }

    public abstract void makeNoyse();

    public void aparearCon(Animal animal) throws ApareamientoInvalidoExcepcion {
        if (this.getClass() != animal.getClass()) {
            throw new ApareamientoInvalidoExcepcion();
        }
        this.pareja = animal;
    }

}

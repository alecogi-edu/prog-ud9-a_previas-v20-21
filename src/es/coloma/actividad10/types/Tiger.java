package es.coloma.actividad10.types;

public class Tiger extends Animal {

    public Tiger(Food food, Size size, String location) {
        super(food, size, location, "Tiger");
    }

    @Override
    public void makeNoyse() {
        System.out.println("OAUGGGGGGG....!");
    }
}

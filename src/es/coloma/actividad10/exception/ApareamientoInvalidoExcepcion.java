package es.coloma.actividad10.exception;

public class ApareamientoInvalidoExcepcion extends Exception {

    public ApareamientoInvalidoExcepcion(){
        super("El apareamiento entre animales de distinta raza no es posible");
    }

    public ApareamientoInvalidoExcepcion(String msg){
        super(msg);
    }

}

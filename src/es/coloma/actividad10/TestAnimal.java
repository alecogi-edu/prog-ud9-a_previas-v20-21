package es.coloma.actividad10;

import es.coloma.actividad10.exception.ApareamientoInvalidoExcepcion;
import es.coloma.actividad10.types.Animal;
import es.coloma.actividad10.types.Lion;
import es.coloma.actividad10.types.Wolf;

/**
 * Actividad11. A partir del código desarrollado en la actividad 6 de la unidad 8 "jerarquía de animales".
 * Modifica las clase Animal y añade el método public void aparearCon(Animal animal)....
 * Este método debe comprobar que los Animales sean de la misma raza, tras lo cual se almacenará en una propiedad el Animal
 * con el que se ha apareado. En caso de que los Animales sean de distinta raza debe generar una excepción
 * ApareamientoInvalidoExcepcion. Añade a la clase TestAnimal un método que se encargue de probar las distintas combinaciones posibles.
 */
public class TestAnimal {

    public static void main(String[] args) {

        Animal lionBig = new Lion(Animal.Food.MEAT, Animal.Size.BIG, "La Sabana");
        Animal wolfBig = new Wolf(Animal.Food.MEAT, Animal.Size.BIG, "La Sabana");
        Animal wolfMedium = new Wolf(Animal.Food.MEAT, Animal.Size.MEDIUM, "La Serreta");

        try {
            System.out.println("Apareando Leon con lobo");
            lionBig.aparearCon(wolfBig);
            System.out.println("OK");
        } catch (ApareamientoInvalidoExcepcion e) {
            System.out.println(e.getMessage());
        }

        try {
            System.out.println("Apareando Lobo con Lobo");
            wolfBig.aparearCon(wolfMedium);
            System.out.println("OK");
        } catch (ApareamientoInvalidoExcepcion e) {
            System.out.println(e.getMessage());
        }

    }

}

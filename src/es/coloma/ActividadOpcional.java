package es.coloma;

/**
 * Activitat6.- Diseña un método llamado dividirEntreArray que reciba como parámetros un número entero y un array de enteros.
 * El método mostrará por pantalla el resultado de la división entre el número recibido y cada uno de los elementos del array.
 * A continuación, escribe un programa que llame al método con el número 2 y un array con los elementos -2, -1, 0, 1 y 2.
 *     • Reescribe el método para capturar la excepción derivada del intento de división entre 0, de forma que no se pare la ejecución del programa y se continúe con la división del resto de elementos del array.
 *     • Crea un segundo método, a partir del anterior, con las sentencias de código necesarias para que la excepción no se llegue a producir.
 */
public class ActividadOpcional {

    public static void main(String[] args) {
        int array[] = { 12, 15, 18, 20, 30,40, 50};
        dividirEntreArray(array,-2);
        dividirEntreArray(array,-1);
        dividirEntreArray(array,0);
        dividirEntreArray(array,1);
        dividirEntreArray(array,2);
        dividirEntreArrayNoException(array,0);
    }

    private static void dividirEntreArray(int array[], int divisor) {
        for (int i = 0; i < array.length; i++ ) {
            try {
                System.out.printf("%d / %d = %d \n", array[i], divisor, array[i]/divisor);
            } catch (ArithmeticException e) {
                System.out.println("Un número no pot ser divisible per 0");
            }
        }
    }

    private static void dividirEntreArrayNoException(int array[], int divisor) {
        for (int i = 0; i < array.length; i++ ) {
            System.out.printf("%d / %d = %.2f \n", array[i], divisor, array[i]/((float) divisor));
        }
    }
}

package es.coloma.actividad12;

import es.coloma.actividad12.exception.InvalidCredentialsException;
import es.coloma.actividad12.exception.MaxAttemptsException;

import java.util.Scanner;

/**
 * Actividad13. A partir del código desarrollado en la actividad 7 de la unidad anterior, crea una clase LoginService con un método public void comprobarCredenciales(String usuario, String password). El método debe lanzar diferentes Excepciones en función del error que se produzca.
 * • CredencialesInvalidasExcepcion: El usuario no existe o existe pero el password no coincide. Deberá dar información al usuario sobre si el error es debido al primer caso o al segundo.
 * • MaximoIntentosAlcanzadosExcepcion: Se ha alcanzado el número máximo de intentos.
 * Crea una clase TestLogin que pida al usuario diferentes combinaciones de usuario/password e informe de los posibles errores que se produzcan.
 * La aplicación finalizará cuando se haya alcanzado el número máximo de intentos (Que vendrá dado por la captura de la excepción MaximoIntentosAlcanzadosExcepcion) o cuando el usuario se loguee correctamente.
 */
public class TestLogin {

    private static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {

        LoginService loginService = new LoginService();
        boolean hasLogged = false;
        do {
            String username = getUsername();
            String password = getPassword();
            try {
                loginService.checkValidCredentials(username, password);
                hasLogged = true;
            } catch (InvalidCredentialsException e) {
                System.out.println("Error: " + e.getMessage());
            } catch (MaxAttemptsException e) {
                System.out.println("Error: " + e.getMessage());
                return;
            }
        } while (!hasLogged);
        System.out.println("Usuario logueado correctamente");
        System.out.println("Bienvenido a la aplicación");
    }

    private static String getUsername() {
       System.out.println("Introduce un usuario:");
       return input.next();
    }

    private static String getPassword() {
        System.out.println("Introduce un password:");
        return input.next();
    }

}

package es.coloma.actividad12;

import es.coloma.actividad12.exception.InvalidCredentialsException;
import es.coloma.actividad12.exception.MaxAttemptsException;

import java.util.HashMap;

public class LoginService {

    private final static int MAX_ATTEMPTS = 3;

    private int attempts;

    private HashMap<String, String> credentials;

    public LoginService() {
        setValidCredentials();
        attempts = 0;
    }

    public void checkValidCredentials(String user, String password) throws InvalidCredentialsException, MaxAttemptsException {
        checkAttempts();
        attempts++;
        if (!credentials.containsKey(user)) {
            checkAttempts();
            throw new InvalidCredentialsException("El usuario introducido no existe");
        } else if (!credentials.get(user).equals(password)) {
            checkAttempts();
            throw new InvalidCredentialsException("El password introducido no es correcto");
        }
        resetAttempts();
    }

    private void checkAttempts() {
        if (!hasPendingAttempts()) {
            throw new MaxAttemptsException();
        }
    }

    private boolean hasPendingAttempts(){
        return this.attempts < MAX_ATTEMPTS;
    }

    private void resetAttempts(){
        this.attempts = 0;
    }

    private void setValidCredentials(){

        this.credentials = new HashMap<>();
        this.credentials.put("admin","admin");
        this.credentials.put("root","123456789");
        this.credentials.put("alecogi","$rtaesr87f");

    }

}

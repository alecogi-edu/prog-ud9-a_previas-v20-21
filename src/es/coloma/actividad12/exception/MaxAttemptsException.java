package es.coloma.actividad12.exception;

public class MaxAttemptsException extends RuntimeException {

    public MaxAttemptsException(){
        super("Ha alcanzado el máximo número de intentos");
    }
    
}

package es.coloma.actividad12.exception;

public class InvalidCredentialsException extends Exception {

    public InvalidCredentialsException(String msg){
        super(msg);
    }
    
}

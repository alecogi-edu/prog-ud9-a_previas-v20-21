package es.coloma.actividad11.excepcion;

public class SaldoAgotadoExcepcion extends Exception {

    public SaldoAgotadoExcepcion(){
        super("No dispone de saldo para subir al autobus");
    }

}

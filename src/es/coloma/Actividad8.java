package es.coloma;

/**
 * Actividad8. Crea un programa llamado WaitApp con una función llamada
 * waitSeconds(int seconds) que recibirá un número de segundos (entero)
 * como parámetro.
 *  Esta función deberá llamar al método Thread.sleep para pausar el programa el
 * número de segundos pasados como parámetro (este método funciona con
 * milisegundos, por lo que se deberá convertir los segundos recibidos a
 * milisegundos).
 * Nota: El método sleep puede producir una excepción
 * InterruptedException , en este caso queremos manejarla desde el método
 * main() (que llamará a la función waitSeconds).
 * Después de esperar el número de segundos especificados, el programa deberá
 * mostrar un mensaje de finalización.
 */
public class Actividad8 {

    public static void main(String[] args) {
        try {
            waitSeconds(3);
            System.out.println("Finalizado");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void waitSeconds(long seconds) throws InterruptedException {
        Thread.sleep(seconds * 1000L);
    }
}

package es.coloma.repaso1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * Actividad 19. 	Crea un programa que pida una fecha en formato DD-MM-YYYY.
 *  Seguidamente indicará si la fecha es mayor a la actual y la mostrará en formato texto
 */
public class Actividad19 {

    public static void main(String[] args) {
        LocalDate fechaIntroducida = askDate();
        showIsActual(fechaIntroducida);
        showDateText(fechaIntroducida);
    }

    public static LocalDate askDate() {
        final DateTimeFormatter dateTimeInputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        final Scanner teclado = new Scanner(System.in);
        do {
            try {
                System.out.println("Introduzca una fecha en formato (dd-mm-yyyy)");
                String fechaTexto = teclado.next();
                return LocalDate.parse(fechaTexto, dateTimeInputFormatter);
            } catch (DateTimeParseException e) {
                System.out.println("Error de formato. Recuerde (dd-mm-yyyy)");
            }
        } while (true);
    }

    public static void showIsActual(LocalDate localDate) {
        LocalDate now = LocalDate.now();
        System.out.printf("La fecha %s es mayor que la actual %n", now.isAfter(localDate) ? "no": "");
    }

    public static void showDateText(LocalDate localDate) {
        final DateTimeFormatter dateTimeOutputFormatter = DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy");
        System.out.println(localDate.format(dateTimeOutputFormatter));
    }

}

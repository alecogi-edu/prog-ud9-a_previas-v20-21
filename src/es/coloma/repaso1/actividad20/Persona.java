package es.coloma.repaso1.actividad20;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;

public class Persona {

    private String firstName;

    private String lastName;

    private LocalDate birthday;

    private String email;

    private String phoneNumber;

    private LocalDateTime createdOn;

    private String password;

    private String salt;

    public Persona(String firstName, String lastName, LocalDate birthday, String email, String phoneNumber, String password, String salt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.createdOn = LocalDateTime.now();
        this.password = password;
        this.salt = salt;
    }

    public int getAge() {
        LocalDate now = LocalDate.now();
        Period anyos = Period.between(birthday, now);
        return anyos.getYears();
    }

    public boolean isNewUser() {
        LocalDateTime fecha = createdOn;
        LocalDateTime dateAfter = fecha.plusDays(15);
        return fecha.isBefore(dateAfter);
    }

}

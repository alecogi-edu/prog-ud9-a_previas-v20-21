package es.coloma.repaso1.actividad20;

import java.time.LocalDate;

public class TestPersona {

    public static void main(String[] args) {

        LocalDate birthday = LocalDate.of(1998, 8, 18);
        Persona sergio = new Persona("Sergio", "Herrero", birthday,
                "batoi_user@gmail.com", "626456497", "daw1234", "prueba");
        System.out.println("La edad es: " + sergio.getAge());
        System.out.println("Es nuevo usuario? -> " + sergio.isNewUser());
    }
}

package es.coloma.repaso1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Actividad 18. Crea una programa que a partir de una fecha y hora del sistema,
 * la muestre en los formatos estándar por defecto (ISO_LOCAL_DATE, ISO_LOCAL_TIME, ISO_LOCAL_DATE_TIME):
 */
public class Actividad18 {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        showDateTimeIsoDate(now);
        showDateTimeIsoTime(now);
        showDateTimeIsoDateTime(now);
    }

    public static void showDateTimeIsoDate(LocalDateTime now) {
        System.out.println(now.format(DateTimeFormatter.ISO_DATE));
    }

    public static void showDateTimeIsoTime(LocalDateTime now) {
        System.out.println(now.format(DateTimeFormatter.ISO_TIME));
    }

    public static void showDateTimeIsoDateTime(LocalDateTime now) {
        System.out.println(now.format(DateTimeFormatter.ISO_DATE_TIME));
    }

}

package es.coloma.repaso1;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Actividad17 {

    private final static int OPTION_DAYS = 1;
    private final static int OPTION_HORAS = 2;
    private final static int OPTION_MINUTES = 3;

    private static Scanner scanner = new Scanner(System.in);

    private static LocalDateTime nowDateTime;

    public static void main(String[] args) {
        nowDateTime = LocalDateTime.now();
        switch (getOption()){
            case OPTION_DAYS :
                addDays();
                break;
            case OPTION_MINUTES :
                addMinutes();
                break;
            case OPTION_HORAS:
                addSeconds();
                break;
            default:
                System.out.println("Opción inválida");
        }
    }

    private static int getOption(){
        System.out.println("Introduce una opción :");
        System.out.println(OPTION_DAYS + ") Añadir días");
        System.out.println(OPTION_HORAS + ") Añadir horas");
        System.out.println(OPTION_MINUTES + ") Añadir minutos");
        return scanner.nextInt();
    }

    private static void addDays(){
        System.out.println("Qué cantidad de días quieres añadir:");
        int days = scanner.nextInt();
        LocalDateTime plusDateTime = nowDateTime.plusDays(days);
        System.out.println(plusDateTime);
    }

    private static void addMinutes(){
        System.out.println("Qué cantidad de minutos quieres añadir:");
        int minutes = scanner.nextInt();
        LocalDateTime plusMinutesDateTime = nowDateTime.plusMinutes(minutes);
        System.out.println(plusMinutesDateTime);
    }

    private static void addSeconds(){
        System.out.println("Qué cantidad de segundos quieres añadir:");
        int seconds = scanner.nextInt();
        LocalDateTime plusSecondsDateTime = nowDateTime.plusSeconds(seconds);
        System.out.println(plusSecondsDateTime);
    }

}


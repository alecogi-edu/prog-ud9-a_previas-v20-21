package es.coloma.repaso2.exception;

public class CanNotMergeBrandsException extends RuntimeException {

    public CanNotMergeBrandsException(){
        super("No se pueden mezclar marcas");
    }
}

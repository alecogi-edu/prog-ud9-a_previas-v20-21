package es.coloma.repaso2.exception;

public class NotExistEnoughtItemException extends RuntimeException {

    public NotExistEnoughtItemException() {
        super("No existen suficientes productos en stock para realizar la compra");
    }

}

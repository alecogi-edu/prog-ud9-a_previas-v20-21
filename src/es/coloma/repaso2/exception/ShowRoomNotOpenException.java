package es.coloma.repaso2.exception;

public class ShowRoomNotOpenException extends Exception {

    public ShowRoomNotOpenException(){
        super("El escaparate se encuentra cerrado");
    }

}

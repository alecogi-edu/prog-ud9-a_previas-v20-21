package es.coloma.repaso2;

public class StockProducto {

    private int unidades;

    private Producto producto;

    public StockProducto(int unidades, Producto producto) {
        this.unidades = unidades;
        this.producto = producto;
    }

    public Producto getProducto() {
        return producto;
    }

    public boolean isAvailable() {
        return unidades > 0;
    }

    public int getUnidades() {
        return unidades;
    }

    public void decrementar(int unidades) {
        this.unidades -= unidades;
    }
}

package es.coloma.repaso2;

import java.util.Objects;

public class Producto implements Comparable<Producto> {

     private String codigo;

     private float precio;

     private Marca marca;

    public Producto(String codigo, float precio, Marca marca) {
        this.codigo = codigo;
        this.precio = precio;
        this.marca = marca;
    }

    public String getCodigo() {
        return codigo;
    }

    public float getPrecio() {
        return precio;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Producto))  {
            return false;
        }
        Producto producto = (Producto) o;
        return codigo.equals(producto.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public int compareTo(Producto o) {

        return o.getCodigo().compareTo(codigo);
    }
}

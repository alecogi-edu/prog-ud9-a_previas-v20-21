package es.coloma.repaso2;

import es.coloma.repaso2.escaparate.EscaparateTemporalFinExistencias;
import es.coloma.repaso2.exception.ShowRoomNotOpenException;

import java.util.ArrayList;

/**
 * Crea una clase TestShopOnline para testear la funcionalidad de las diferentes clases creadas.
 * Deberá llevar a cabo las siguientes acciones:
 * ➔ Crea los siguientes productos de marca Lacostera;
 * 1. “polorayas”de50euros.
 * 2. “bañadorazul”de40euros
 * ➔ Crea el escaparate temporal hasta fin de existencias (escaparate) con nombre
 * “verano Lacostera 2020”, marca “Lacostera” y productos en venta el polo a
 * rayas y el bañador azul.
 * ➔ Muestra los productos del escaparate ordenados alfabéticamente.
 * ➔ Consulta y muestra las unidades en stock del polo a rayas y del bañador
 * ➔ Consulta y muestra el tiempo que queda para que el escaparate esté abierto
 * ➔ Haz una compra de todas las unidades del polo y del bañador
 * ➔ Consulta y muestra si el escaparate está abierto
 */
public class TestShopOnline {

    public static void main(String[] args) {

        ArrayList<Producto> listado = new ArrayList<>();
        Producto poloARayas = new Producto("Polo a rayas", 50f, Marca.LA_COSTERA);
        Producto banyadorAzul = new Producto("bañadorazul", 40f, Marca.LA_COSTERA);

        listado.add(poloARayas);
        listado.add(banyadorAzul);

        EscaparateTemporalFinExistencias e1 = new EscaparateTemporalFinExistencias("verano Lacostera 2020",
                Marca.LA_COSTERA, listado);

        //Consulta y muestra las unidades en stock del polo a rayas y del bañador
        System.out.println(e1.getUnidadesEnVenta(poloARayas));
        System.out.println(e1.getUnidadesEnVenta(banyadorAzul));

        //Consulta y muestra el tiempo que queda para que el escaparate esté abierto
        System.out.printf("Quedan %d dias para cerrar %n", e1.getDiasRestanteApertura());

        // Haz una compra de todas las unidades del polo y del bañador
        try {
            e1.comprar(poloARayas, e1.getUnidadesEnVenta(poloARayas));
            e1.comprar(banyadorAzul, e1.getUnidadesEnVenta(banyadorAzul));
        } catch (ShowRoomNotOpenException e) {
            e.printStackTrace();
        }

        //Consulta y muestra si el escaparate está abierto
        System.out.println(e1.isAbierto());

    }
}

package es.coloma.repaso2.escaparate;

import es.coloma.repaso2.Marca;
import es.coloma.repaso2.Producto;
import es.coloma.repaso2.StockProducto;

import java.util.List;

public class EscaparateFinExistencias extends Escaparate{

    private boolean cerrado;

    public EscaparateFinExistencias(String nombre, Marca marca, List<Producto> listadoProductos) {
        super(nombre, marca, listadoProductos);
    }

    public void cerrar() {
        cerrado = true;
    }

    @Override
    public boolean isAbierto() {
        return (!cerrado && quedanProductosStock());
    }

    private boolean quedanProductosStock() {
        for (StockProducto lineaProducto: listadoProductos) {
            if (lineaProducto.isAvailable()) {
                return true;
             }
        }
        return false;
    }
}

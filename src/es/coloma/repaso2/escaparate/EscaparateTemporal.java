package es.coloma.repaso2.escaparate;

import es.coloma.repaso2.Marca;
import es.coloma.repaso2.Producto;

import java.util.List;

/**
 * - Escaparate temporal: se caracteriza porque está abierto desde su creación un número limitado de días.
 * Los días de apertura del escaparate se establecen en el constructor y no podrán cambiar.
 * El escaparate se dice que está abierto mientras quede algún segundo.
 * Se puede consultar los días que le queda de estar abierto.
 * Para la gestión del tiempo se puede utilizar el método currentTimeMillis de la clase System. Este método devuelve un instante de tiempo en milisegundos, por lo que puedes calcular la diferencia de tiempo desde la que se creó el escaparate hasta el momento en el que se esta realizando la compra para saber si permanece abierto. Recuerda que un día tiene 86400 segundos y 1 segundo 1000 milisegundos.
 */
public class EscaparateTemporal extends Escaparate{

    private final int SEGUNDOS_DIAS = 86400;

    private int diasApertura;

    private long creadoEnTimestamp;

    public EscaparateTemporal(String nombre, Marca marca,
                              List<Producto> listadoProductos, int diasApertura) {
       super(nombre, marca, listadoProductos);
       this.diasApertura = diasApertura;
       this.creadoEnTimestamp = System.currentTimeMillis();
    }

    @Override
    public boolean isAbierto() {
       return getDiasTranscurridos() < diasApertura;
    }

    public int getDiasTranscurridos() {
        long timestampActual = System.currentTimeMillis();
        long segundosTranscurridos = (creadoEnTimestamp - timestampActual) / 1000;
        return (int) segundosTranscurridos / SEGUNDOS_DIAS;
    }

    public int getDiasRestanteApertura() {
        return diasApertura - getDiasTranscurridos();
    }

}

package es.coloma.repaso2.escaparate;

import es.coloma.repaso2.Marca;
import es.coloma.repaso2.Producto;
import es.coloma.repaso2.StockProducto;
import es.coloma.repaso2.exception.NotExistEnoughtItemException;
import es.coloma.repaso2.exception.ShowRoomNotOpenException;
import es.coloma.repaso2.exception.CanNotMergeBrandsException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public abstract class Escaparate {

    private String nombre;

    private Marca marca;

    private Random generadorAleatorios;

    protected ArrayList<StockProducto> listadoProductos = new ArrayList<>();

    public Escaparate(String nombre, Marca marca, List<Producto> listadoProductos) {
        this.nombre = nombre;
        this.marca = marca;
        this.generadorAleatorios = new Random();
        anyadirProductos(listadoProductos);
    }

    public abstract boolean isAbierto();

    public List<Producto> getProductosOrdenados() {
        List<Producto> productosDisponibles = new ArrayList<>();
        for (StockProducto lineaProducto: listadoProductos) {
            productosDisponibles.add(lineaProducto.getProducto());
        }
        Collections.sort(productosDisponibles);
        return productosDisponibles;
    }

    public List<Producto> getProductosEnVenta(){
        List<Producto> productosDisponibles = new ArrayList<>();
        for (StockProducto lineaProducto: listadoProductos) {
            if (lineaProducto.isAvailable()) {
                productosDisponibles.add(lineaProducto.getProducto());
            }
        }
        return productosDisponibles;
    }

    public boolean getProductosEnVenta(Producto producto, int unidades) {
        StockProducto stockProducto = getStockProducto(producto);
        if (stockProducto != null) {
            return stockProducto.getUnidades() >= unidades;
        }
        return false;
    }

    public boolean isProductoEnVenta(Producto producto){
        StockProducto stockProducto = getStockProducto(producto);
        if (stockProducto != null) {
            return stockProducto.isAvailable();
        }
        return false;
    }

    public int getUnidadesEnVenta(Producto producto) {
        StockProducto stockProducto = getStockProducto(producto);
        if (stockProducto != null) {
            return stockProducto.getUnidades();
        }
        return 0;
    }

    private StockProducto getStockProducto(Producto producto) {
        for (StockProducto lineaProducto: listadoProductos) {
            if (lineaProducto.getProducto().equals(producto)) {
                return lineaProducto;
            }
        }
        return null;
    }

    public void comprar(Producto producto, int unidades) throws ShowRoomNotOpenException {
        if (!isAbierto()) {
            throw new ShowRoomNotOpenException();
        }
        if (getUnidadesEnVenta(producto) < unidades) {
            throw new NotExistEnoughtItemException();
        }
        StockProducto stockProducto = getStockProducto(producto);
        stockProducto.decrementar(unidades);
    }

    private void anyadirProductos(List<Producto> productos) {
        for (Producto producto: productos) {
            if (producto.getMarca() != marca) {
                throw new CanNotMergeBrandsException();
            }
            this.listadoProductos.add(new StockProducto(getCantidadAleatoria(), producto));
        }
    }

    private int getCantidadAleatoria() {
        final int MAX_CANTIDAD = 10;
        return generadorAleatorios.nextInt(MAX_CANTIDAD);
    }

}

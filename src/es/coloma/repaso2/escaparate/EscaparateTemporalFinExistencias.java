package es.coloma.repaso2.escaparate;

import es.coloma.repaso2.Marca;
import es.coloma.repaso2.Producto;

import java.util.List;

public class EscaparateTemporalFinExistencias extends EscaparateTemporal {

    public EscaparateTemporalFinExistencias(String nombre, Marca marca,
                                            List<Producto> listadoProductos) {
       super(nombre, marca, listadoProductos, 3);
    }

    @Override
    public boolean isAbierto() {
        return super.isAbierto() && super.getProductosEnVenta().size() > 0;
    }

}
